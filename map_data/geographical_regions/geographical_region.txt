﻿# Geographical regions
# Regions can be declared with one or more of the following fields:
#	duchies = { }, takes duchy title names declared in landed_titles.txt
#	counties = { }, takes county title names declared in landed_titles.txt
#	provinces = { }, takes province id numbers declared in /history/provinces
#	regions = { }, a region can also include other regions, however the subregions needs to be declared before the parent region. 
#		E.g. If the region world_europe contains the region world_europe_west then world_europe_west needs to be declared as a region before (i.e. higher up in this file) world_europe.

###########################################################################
# World Regions
#	These groups are mutually exclusive on the same tier & should cover every part of the map
###########################################################################

# Cannor

world_cannor_gerudia = {
	duchies = {
		# Bjarnrik
		d_bjarnland d_bifrutja d_revrland d_haugrmyrr d_sarbann d_kaldrland d_alptborg d_sidaett d_ismark
		# Obrtrol #disabled until we have trolls
		# d_dalrfjall d_thurrsbol
		# Rimurhals
		d_drekkiskali d_olavslund d_jotunhamr
		# Urviksten
		d_esfjall d_naugsvol d_avnkaup
		# Jotuntar
		d_norjotuntar d_sudjotuntar
	}
}

world_cannor_west_lencenor = {
	duchies = {
		# Lorent
		d_lorentaine d_rosefield d_ainethan d_redglades d_rewanwood d_upper_bloodwine d_lower_bloodwine
		# Deranne
		d_deranne d_darom
		# Enteben
		d_enteben d_great_ording d_horsegarden d_crovania
		# Sorncost
		d_sorncost d_sormanni_hills d_coruan d_venail
		# Rubyhold
		d_rubyhold
		# Iochand
		d_iochand d_portnamm d_southroy
		# Tretun
		d_tretun d_roilsard
		
		d_viswall d_barrowshire d_greymill d_thomsbridge
		d_elkmarch
		d_roysfort d_bigwheat
		d_pearview d_appleton
		d_reaver_coast d_gnomish_pass d_nimscodd
	}
}

world_cannor_west_dameshead = {
	duchies = {
		# Dameria
		d_damesear d_wesdam d_neckcliffe d_exwes d_istralore d_silverwoods d_acromton d_plumwall d_upper_luna d_heartlands
		# Carneter
		d_carneter
		# Pearlsedge
		d_pearlsedge d_pearlywine
		# Verne
		d_verne d_wyvernmark d_the_tail d_eastneck d_menibor_loop d_armanhal d_galeinn
		# Esmaria
		d_estallen d_konwell d_bennon d_leslinpar d_cann_esmar d_ryalanar d_songbarges d_asheniande d_hearthswood d_silverforge
		# Wex
		d_wexhills d_escandar d_bisan d_ottocam d_greater_bardswood d_sugamber
		# The Borders
		d_gisden d_brinkwick d_arannen d_celliande d_toarnen d_antirhal d_tellum d_hawkfields
		# Damescrown
		d_damescrown d_aranmas
		# Vertesk
		d_vertesk d_floodmarshes
		# Uelaire
		d_uelaire
		# Beepeck
		d_beepeck
		# Busilar
		d_lioncost d_busilari_straits d_lorin d_canno d_stonehold d_hortosare d_lorbet d_hapaine
		# Eborthil
		d_eborthil d_tefkora
	}
}

world_cannor_west_alenor = {
	duchies = {
		# Gawed
		d_gawed d_westmounts d_ginnfield d_alenic_expanse d_greatmarch d_balvord d_oudescker d_arbaran d_freecestir
		# Eaglecrest
		d_dragonhills d_fluddhill
		# Westmoors
		d_westmoor d_moorhills d_beronmoor
		# Adshaw
		d_adshaw d_bayvic d_west_chillsbay d_adderwood d_serpentgard d_celmaldor
		# Coddoran
		# d_coddorran
		d_storm_isles
		
		d_crodam d_teagansfield
	}
}

world_cannor_west = {
	regions = {
		world_cannor_west_lencenor world_cannor_west_dameshead world_cannor_west_alenor
	}
}

world_cannor_east_castanor = {
	duchies = {
		# Castonath
		d_castonath
		# Cast
		d_trialmount d_steelhyl d_serpentsmarck d_northyl d_westgate d_nath
		# Sarwick
		d_sarwood d_kondunn d_nortessord d_esshyl
		# Anor
		d_ardent_glade d_sapphirecrown d_nortmere d_oldhaven d_bradsecker d_foarhal d_southgate
		# Marrhold
		d_marrhold d_dryadsdale d_doewood d_hornwood
		# Blademarches
		d_blademarch d_medirleigh d_swapstroke d_clovenwood d_beastgrave
		# Humacvord
		d_humacvord d_wallor d_themin d_burnoll
		# Devaced
		d_devaced d_vernham d_dostans_way
		# Adenica
		d_adenica d_rohibon d_verteben d_taran_plains d_acengard d_valefort
		# Merewood
		d_merescker d_nortmerewood d_oudmerewood
		# Balmire
		d_bal_mire d_falsemire d_middle_alen
		# Agrador
		d_cannwood d_agradalen
		# Vrorenmarch
		d_vrorenwall d_cedesck d_sondaar d_wudhal d_east_chillsbay d_ebonmarck d_tencfor
		# Ibevar
		d_ibevar d_nurael d_larthan d_whistlevale d_cursewood
	}
}

world_cannor_east_dostanor = {
	duchies = {
		# Corvuria
		d_bal_dostan d_blackwoods d_tiferben d_ravenhill
		# Corveld
		d_corveld d_dreadmire
		# Ourdia
		d_tencmarck d_lencmarck d_oudmarck
	}
}

world_cannor_east = {
	regions = {
		world_cannor_east_castanor world_cannor_east_dostanor
	}
}

world_cannor = {
	regions = {
		world_cannor_west world_cannor_east world_cannor_gerudia
	}
}

world_europe = {
	regions = {
		world_cannor_west world_cannor_east world_cannor_gerudia
	}
}

# Bulwar

world_sarhal_bulwar_north = {
	duchies = {
		# Re'uyel
		d_reuyel d_crathanor d_medbahar
		# Ovdal Tungr
		d_ovdal_tungr
		# Bahar
		d_aqatbahar d_magairous d_birsartenslib
		d_bahar_szel_uak d_azka_evran d_azkasad
		# Gelkalis
		d_gelkalis d_gelkarzan d_elusadul
		# Harpylen
		d_harpylen d_alyzksaan d_crelyore
		# Firanyalen
		d_firanyalen d_uvolate d_hranapas
	}
}

world_sarhal_bulwar_central = {
	duchies = {
		# Bulwar
		d_bulwar d_zanbar d_harklum d_kalib
		d_zansap d_nabasih d_sareyand
		# Imulušes
		d_imuluses d_sad_kuz
		# Idanas
		d_idanas d_anzabad
		# Brasan
		d_sap_brasan d_baru_brasan d_len_brasan
		# Drolas
		d_arag_drolas d_baru_drolas
		# Sad Sur
		d_sad_sur d_agshilsu d_ekluzagnu d_ardutibad
		d_tumerub
		# Kumaršes
		d_kumarses d_varamhar d_anzarzax
		# Akalšes
		d_akalses d_azkabar d_setadazar d_kalisad
		# Avamezan
		d_avamezan
		# Hašr
		d_hasr d_panuses d_eduz_vacyn
	}
}

world_sarhal_bulwar_east = {
	duchies = {
		# Azka Sur
		d_azka_sur d_jikarzax d_nabilsu
		# Seghdihr
		d_seghdihr
		# Siadanlen
		d_siadanlen d_harpyget d_golrod
		d_endenn d_salahimil d_ebbusubtu
		# Verkal Gulan
		d_verkal_gulan
		# Apaših
		d_keruhar d_kesudsah d_saranza d_arsagnu
	}
}

world_sarhal_bulwar = {
	regions = {
		world_sarhal_bulwar_north world_sarhal_bulwar_central world_sarhal_bulwar_east
	}
}

world_sarhal_salahad_akasik = {
	duchies = {
		# Ekha
		d_ekha d_akarat
		# Khasa
		d_khasa d_ikasakan
		# Deshak
		d_deshak d_hapak
		# Ayshanaz
		d_krahkeysa d_axakmoz d_boozazn
	}
}

world_sarhal_salahad_kheterata = {
	duchies = {
		# Kheterata
		d_kheterat d_golkora d_awaashesh
		d_masusopot d_ibtat_axast d_ibtatu
		d_nirat d_hitputtiushesh d_anarat
		d_aakheta d_sopotremit d_ohitsopot
		# Irsmahap
		d_dawimshesh d_hapmot d_miugesh
	}
}

world_sarhal_salahad = {
	regions = {
		world_sarhal_salahad_kheterata world_sarhal_salahad_akasik
	}

	duchies = {
		# Harragrar
		d_harra d_ardu d_krahway d_gnolltauz
		# Krahwix
		d_krahilzin d_saymas d_koggraffa
		# Dasmazar
		d_udamsah d_nolhdrus d_dasmasih
	}
}

world_sarhal = {
	regions = {
		world_sarhal_bulwar world_sarhal_salahad
	}
}

###########################################################################
# Material Regions
###########################################################################

#TODO

###########################################################################
# Custom Regions
###########################################################################

custom_inner_castanor = {
	duchies = {
		#Cast
		d_trialmount d_steelhyl d_serpentsmarck d_northyl d_westgate d_nath 
		#Anor
		d_ardent_glade d_sapphirecrown d_nortmere d_oldhaven d_bradsecker d_foarhal d_southgate 
		#Castonath
		d_castonath
	}
}

custom_cannor_reach = {
	duchies = {
		#Adshaw
		d_serpentgard d_celmaldor d_adderwood d_adshaw d_bayvic d_west_chillsbay 
		#Vrorenmarch
		d_east_chillsbay d_sondaar d_wudhal d_cedesck d_vrorenwall 
	}
}

custom_arbaran = {
	duchies = {
		d_arbaran
		d_freecestir
		d_crodam
		d_teagansfield
	}
}

custom_cannor_westmoors = {
	duchies = {
		d_westmoor
		d_moorhills
		d_beronmoor
	}
}

custom_small_country = {
	duchies = {
		# Viswall
		d_viswall d_barrowshire d_greymill d_thomsbridge
		#Elkmarch
		d_elkmarch
		# Roysfort
		d_roysfort d_bigwheat
		# Pearlview
		d_pearview d_appleton
		d_beepeck
	}
}

###########################################################################
# Graphical Regions
###########################################################################

# Anbenanr: decide on these

graphical_western = {
	graphical = yes
	color = { 255 0 0 }
	regions = {
		world_cannor_gerudia world_cannor_west_alenor world_cannor_east_castanor
	}
	duchies = { # all this shit should be in regions, custom or not
		d_upper_luna
		
		d_lorentaine d_rosefield d_redglades d_rewanwood
		
		d_rubyhold
		d_iochand d_portnamm d_southroy
		
		d_viswall d_barrowshire d_greymill d_thomsbridge
		d_elkmarch
		d_roysfort d_bigwheat
		d_pearview d_appleton
		d_reaver_coast d_gnomish_pass d_nimscodd
		
		d_estallen d_bennon d_leslinpar d_asheniande d_hearthswood d_silverforge
		d_wexhills d_escandar d_bisan d_ottocam d_greater_bardswood d_sugamber
		d_gisden d_brinkwick d_arannen d_antirhal d_tellum
		
		d_bal_dostan d_blackwoods d_tiferben d_ravenhill
		d_dreadmire
	}
}

graphical_mena = {
	graphical = yes
	color = { 255 255 0 }
	regions = {
		world_sarhal
	}
}

# graphical_india = {
	# graphical = yes
	# color = { 0 255 0 }
	# regions = {
		# world_india world_tibet world_burma
	# }
# }

graphical_mediterranean = {
	graphical = yes
	color = { 0 0 255 }
	duchies = { # all this shit should be in regions, custom or not
		d_konwell d_cann_esmar d_ryalanar d_songbarges
		d_damesear d_wesdam d_neckcliffe d_exwes d_istralore d_silverwoods d_acromton d_plumwall d_heartlands
		d_carneter
		d_pearlsedge d_pearlywine
		d_verne d_wyvernmark d_the_tail d_eastneck d_menibor_loop d_armanhal d_galeinn
		d_damescrown d_aranmas
		d_vertesk d_floodmarshes
		d_uelaire
		d_beepeck
		d_lioncost d_busilari_straits d_lorin d_canno d_stonehold d_hortosare d_lorbet d_hapaine
		d_eborthil d_tefkora
		
		d_tretun d_roilsard
		d_ainethan d_upper_bloodwine d_lower_bloodwine
		d_sorncost d_sormanni_hills d_coruan d_venail
		d_enteben d_great_ording d_horsegarden d_crovania
		d_deranne d_darom
		
		d_tencmarck d_lencmarck d_oudmarck
		d_corveld
		
		d_celliande d_toarnen d_hawkfields
	}
}

# graphical_steppe = {
	# graphical = yes
	# color = { 0 255 255 }
	# regions = {
		# world_steppe
	# }
# }

##############################
# Misc
###############################

world_innovation_elephants = {
	generate_modifiers = yes
	duchies = { # until stuff is made better
		# Re'uyel
		d_reuyel d_crathanor d_medbahar
		# Bahar
		d_aqatbahar d_magairous d_birsartenslib
		d_bahar_szel_uak d_azka_evran d_azkasad
	}
}

world_innovation_camels = {
	generate_modifiers = yes
	regions = {
		world_sarhal
	}
}

### SEAZONES ###

# TODO

### Hunts

# Deer Spread
hunt_animal_deer_region = {
	duchies = {
		d_damesear # placeholder
	}
}

# Antelope Spread
hunt_animal_antelope_region = {
	duchies = {
		d_damesear # placeholder
	}
}

# Gazelle Spread
hunt_animal_gazelle_region = {
	duchies = {
		d_damesear # placeholder
	}
}

# Boar Spread
hunt_animal_boar_region = {
	duchies = {
		d_damesear # placeholder
	}
}

# Bear Spread
hunt_animal_bear_region = {
	duchies = {
		d_damesear # placeholder
	}
}

# Big Cat Spread
hunt_animal_big_cat_region = {
	duchies = {
		d_damesear # placeholder
	}
}

# Bison Spread
hunt_animal_bison_region = {
	
}

# Aurochs Spread
hunt_animal_aurochs_region = {
	duchies = {
		d_damesear # placeholder
	}
}

hunt_animal_reindeer_region = {
	duchies = {
		d_damesear # placeholder
	}
}

hunt_animal_elk_region = {
	duchies = {
		d_damesear # placeholder
	}
}

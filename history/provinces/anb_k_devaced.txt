#k_devaced
##d_devaced
###c_devaced
797 = {		#Devaced

	# Misc
	culture = devacedi
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2317 = {

    # Misc
    holding = city_holding

    # History

}
2318 = {

    # Misc
    holding = church_holding

    # History

}
2319 = {

    # Misc
    holding = none

    # History

}

###c_diremill
798 = {		#Diremill

	# Misc
	culture = devacedi
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2320 = {

    # Misc
    holding = city_holding

    # History

}
2321 = {

    # Misc
    holding = none

    # History

}

###c_godsescker
791 = {		#Godsescker

	# Misc
	culture = devacedi
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2316 = {

    # Misc
    holding = none

    # History

}

###c_catelsvord
799 = {		#Catelsvord

	# Misc
	culture = devacedi
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2322 = {

    # Misc
    holding = church_holding

    # History

}
2323 = {

    # Misc
    holding = none

    # History

}

##d_vernham
###c_vernham
800 = {		#Vernham (Old Vernham)

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2324 = {

    # Misc
    holding = church_holding

    # History

}
2325 = {

    # Misc
    holding = city_holding

    # History

}
2326 = {

    # Misc
    holding = none

    # History

}
2327 = {

    # Misc
    holding = none

    # History

}

###c_oudeben
801 = {		#Oudeben

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2336 = {

    # Misc
    holding = church_holding

    # History

}

###c_mereham
809 = {		#Mereham

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2332 = {

    # Misc
    holding = church_holding

    # History

}
2333 = {

    # Misc
    holding = city_holding

    # History

}
2335 = {

    # Misc
    holding = none

    # History

}

###c_arrowfield
810 = {		#Arrowfield

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2330 = {

    # Misc
    holding = city_holding

    # History

}
2331 = {

    # Misc
    holding = none

    # History

}

###c_mesenhome
808 = {		#Mesenhome (Vernham)

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2328 = {

    # Misc
    holding = city_holding

    # History

}
2329 = {

    # Misc
    holding = none

    # History

}

##d_dostans_way
###c_dostans_way
803 = {		#Dostan's Way

	# Misc
	culture = devacedi
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
802 = {		#Woodmarck

	# Misc
	holding = church_holding

	# History
}
2311 = {

    # Misc
    holding = none

    # History

}

###c_guidesway
807 = {		#Guidesway

	# Misc
	culture = devacedi
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2314 = {

    # Misc
    holding = church_holding

    # History

}
2315 = {

    # Misc
    holding = none

    # History

}

###c_aldcamp
805 = {		#Aldcamp

	# Misc
	culture = devacedi
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2312 = {

    # Misc
    holding = city_holding

    # History

}
2313 = {

    # Misc
    holding = none

    # History

}

###c_glademarch
804 = {		#Glademarch

	# Misc
	culture = devacedi
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2309 = {

    # Misc
    holding = none

    # History

}
2310 = {

    # Misc
    holding = none

    # History

}
